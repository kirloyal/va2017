#block(name=extract-features, threads=1, memory=10000, gpus=0, hours=4)

    mkdir -p log results

    for PART in test train; do

        OPTIONS="--log-file=log/$PART.dt.log \
                 --features.feature-writer.feature-cache=results/$PART.dt.gz \
                 --dt.video-list=$PART.videos"

        ../../src/ActionRecognition/action-recognizer --config=config/dt.config $OPTIONS

    done
