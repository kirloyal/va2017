/*
 * DenseTraj.hh Created by Oh-Hun Kwon
 *	based on Stip.hh  *  Created on: Apr 25, 2017 Author: richard
 *      
 */

#ifndef ACTIONRECOGNITION_DENSE_TRAJ_HH_
#define ACTIONRECOGNITION_DENSE_TRAJ_HH_

#include "Core/CommonHeaders.hh" // Core::Log, Core::Error, Core::Configuration, Types
#include "Math/Matrix.hh"
#include "Math/Vector.hh"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>

namespace ActionRecognition {

class DenseTraj
{
private:
	struct Point {
		u32 x, y, t;
		Point(u32 _x, u32 _y, u32 _t) { x = _x; y = _y; t = _t; }
	};
	struct InterestPoint : Point {
		Float score;
		InterestPoint(u32 _x, u32 _y, u32 _t, Float _score) : Point(_x, _y, _t) { score = _score; }
	};

	// used for efficient access to cv::Mat
	struct MemoryAccessor {
		u32 rows, cols;
		Float* mem;
		MemoryAccessor(u32 r, u32 c, Float* m) { rows = r; cols = c; mem = m; }
		Float operator()(u32 x, u32 y) const  { return mem[x * cols + y]; }
	};
	// compare the scores of two interest points (for std::sort)
	static bool compareInterestPoints(const InterestPoint i, const InterestPoint j) { return i.score > j.score; }

	static const Core::ParameterString paramVideoList_;
	static const Core::ParameterString paramIsShow;
	std::string videoList_;
	std::string isplay_;
	

	typedef std::vector<cv::Mat> Video;

	struct Trajectory {
		std::vector<cv::Point> vecPoint;
		u32	tStart;
		Trajectory(cv::Point pointInit, u32 t) :
			tStart(t)
		{
			vecPoint.push_back(pointInit);
		}
	};

	typedef std::vector< std::vector< Trajectory > > Trajectories;	// time axis, list
	


    
    // void ExtractDT(const Video& video, const Video& flowX, const Video& flowY, Trajectories& trajs);
    void DenseSampling(const Video& video, Trajectories& trajs, u32 W = 5);
    void TrackTraj(const Video& video, const Video& flowX, const Video& flowY, Trajectories& trajs, u32 L = 15);
    void PruneTraj(Trajectories& trajs);
    void GetDescripter(const Video& video, const Video& gradAngle, const Video& gradMag,
    	const Video& flowAngle, const Video& flowMag, const Trajectory traj,
    	std::vector< Math::Vector<Float> >& descriptor,
    	u32 N = 32, u32 L = 15, u32 ns = 2, u32 nt = 3);
	void OpticalFlow(const Video& video, Video& flowX, Video& flowY);
	void Derivatives2D(const Video& in, Video& Lx, Video& Ly);
	void Derivatives3D(const Video& in, Video& Lx, Video& Ly, Video& Lt);
	void ConvertCartToPolar(const Video& x, const Video& y, Video& angle, Video& mag);

	void GetDescripter(const Video& video, const Trajectory& traj,
		const Video& gradAngle, const Video& gradMag,
    	const Video& flowAngle, const Video& flowMag, 
    	const Video& LxFAngle, const Video& LxFMag,
    	const Video& LyFAngle, const Video& LyFMag,
    	Math::Vector<Float>& descriptor,
    	u32 N = 32, u32 L = 15, u32 ns = 3, u32 nt = 2);

	void ReadVideo(const std::string& filename, Video& result, Video& raw);
	void ShowWithDT(const Video& video, const Trajectories& trajs, u32 L = 15);
	
	// void opticalFlow(const Video& video, Video& flowAngle, Video& flowMag);
	void filter3d(const Video& in, Video& out, Float sigma, Float tau);
	
	


public:
	DenseTraj();
	virtual ~DenseTraj();
	void run();
};

} // namespace


#endif /* ACTIONRECOGNITION_DENSE_TRAJ_HH_ */
