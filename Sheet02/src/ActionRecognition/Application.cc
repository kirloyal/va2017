/*
 * Application.cc
 *
 *  Created on: Jun 29, 2015
 *      Author: richard
 */

#include "Application.hh"
#include <iostream>
#include "Example.hh"
#include "DenseTraj.hh"
#include "Math/Random.hh"
#include "KMeans.hh"
#include "Svm.hh"

using namespace ActionRecognition;

APPLICATION(ActionRecognition::Application)

const Core::ParameterEnum Application::paramAction_("action",
		"example, dt, svm-train, svm-classify", // list of possible actions
		"example"); // the default

void Application::main() {

	switch (Core::Configuration::config(paramAction_)) {
	case example:
	{
		Example example;
		example.run();
	}
	break;
	case dt:
	{
		DenseTraj denseTraj;
		denseTraj.run();
	}
	break;
	case svmTrain:
	{
		Svm svm;
		svm.train();
	}
	break;
	case svmClassify:
	{
		Svm svm;
		svm.classify();
	}
	break;
	default:
		Core::Error::msg("No action given.") << Core::Error::abort;
	}
}
