/*
 * DenseTraj.cc Created by Oh-Hun Kwon
 *	based on Stip.cc  *  Created on: Apr 25, 2017 Author: richard
 *      
 */

#include "DenseTraj.hh"
#include <algorithm>
#include "Features/FeatureWriter.hh"

using namespace ActionRecognition;

const Core::ParameterString DenseTraj::paramVideoList_("video-list", "", "dt");
const Core::ParameterString DenseTraj::paramIsShow("play", "false", "dt");

// constructor
DenseTraj::DenseTraj() :
		videoList_(Core::Configuration::config(paramVideoList_)),
		isplay_(Core::Configuration::config(paramIsShow))
{}
	
// empty destructor
DenseTraj::~DenseTraj()
{}


void DenseTraj::run() {
	std::cout << "Start DenseTraj::run()" << std::endl;  // DEBUG
	if (videoList_.empty())
		Core::Error::msg("dt.video-list must not be empty.") << Core::Error::abort;

	Core::AsciiStream in(videoList_, std::ios::in);
	std::string strVideo;
	std::vector< Math::Matrix<Float> > features;
	u32 totalNumberOfVectors = 0;

	/* extract features for each video */
	while (in.getline(strVideo))
    {
		std::cout << strVideo << std::endl;
		
		Video video, videoRaw;
		ReadVideo(strVideo, video, videoRaw);

		Video flowX, flowY;
		OpticalFlow(video, flowX, flowY);
		
		Trajectories trajs;
		DenseSampling(video, trajs);	// get initial points of trajectories bt dense sampling
		TrackTraj(video, flowX, flowY, trajs);
		PruneTraj(trajs);

	    if(isplay_ == "true") ShowWithDT(videoRaw, trajs);
	    	

		Video Lx, Ly, Lt, gradAngle, gradMag;
	    Video flowAngle, flowMag;
		Derivatives3D(video, Lx, Ly, Lt);
		ConvertCartToPolar(Lx, Ly, gradAngle, gradMag);
		ConvertCartToPolar(flowX, flowY, flowAngle, flowMag);

		Video LxFx, LyFx, LxFy, LyFy;
		Video LxFAngle, LxFMag, LyFAngle, LyFMag;
		Derivatives2D(flowX, LxFx, LyFx);
		Derivatives2D(flowY, LxFy, LyFy);
		ConvertCartToPolar(LxFx, LxFy, LxFAngle, LxFMag);
		ConvertCartToPolar(LyFx, LyFy, LyFAngle, LyFMag);

		
		std::vector< Math::Vector<Float> > descriptors;
		
		for (u32 t = 0; t < trajs.size(); t++) 
		{
			for(u32 i=0 ; i < trajs[t].size() ; i++)
			{
				Math::Vector<Float> oneDscr;
				GetDescripter(video, trajs[t][i], gradAngle, gradMag, flowAngle, flowMag,
					LxFAngle, LxFMag, LyFAngle, LyFMag, oneDscr);
		    	if (oneDscr.asum() > 0)
					descriptors.push_back(oneDscr);
			}
		}

        if(descriptors.size() > 0)
        {
            features.push_back(Math::Matrix<Float>(descriptors[0].nRows(), descriptors.size()));
		    for (u32 i = 0; i < descriptors.size(); i++)
			    Math::copy(descriptors.at(i).size(), descriptors.at(i).begin(), 1, features.back().begin() + descriptors.at(i).size() * i, 1);
		    totalNumberOfVectors += descriptors.size();
        }
    }

	/* write extracted features to an output file */
	Features::SequenceFeatureWriter writer;
	writer.initialize(totalNumberOfVectors, 2*15 + 3*3*2*(8+9+8+8), features.size());
	for (u32 i = 0; i < features.size(); i++)
		writer.write(features.at(i));
	std::cout << "End DenseTraj::run()" << std::endl;  // DEBUG
}



void DenseTraj::DenseSampling(const Video& video, Trajectories& trajs, u32 W)
{
	std::cout << "Start DenseTraj::DenseSampling" << std::endl;  // DEBUG
	for(u32 t=0 ; t<video.size() ; t++)
	{
		cv::Mat imgMinEV;
		cornerMinEigenVal(video[t], imgMinEV, 3);
		double min, max;
		cv::minMaxLoc(imgMinEV, &min, &max);
		float T = 0.001 * max;

		MemoryAccessor maMinEV(imgMinEV.rows, imgMinEV.cols, (Float*)imgMinEV.data);
		std::vector< Trajectory > trajInFrame;
		for(u32 y=0 ; y < (u32)video[t].rows ; y += W)
		{
			for(u32 x=0 ; x < (u32)video[t].cols ; x += W)
			{
				if( maMinEV(x,y) > T)
				{
					trajInFrame.push_back(Trajectory(cv::Point(x,y), t));
				}
			}
		}
		trajs.push_back(trajInFrame);
	}
	std::cout << "End DenseTraj::DenseSampling" << std::endl;  // DEBUG	
}


void DenseTraj::TrackTraj(const Video& video, const Video& flowX, const Video& flowY, Trajectories& trajs, u32 L)
{
	std::cout << "Start DenseTraj::TrackTraj()" << std::endl;  // DEBUG
	for(u32 t = 0; t<video.size()-1; t++)
	{
		cv::Mat flow;
				
		cv::Mat flowMedX, flowMedY;
		cv::medianBlur(flowX[t], flowMedX, 3);
		cv::medianBlur(flowY[t], flowMedY, 3);
		MemoryAccessor maU(flowMedX.rows, flowMedX.cols, (Float*)flowMedX.data);
		MemoryAccessor maV(flowMedY.rows, flowMedY.cols, (Float*)flowMedY.data);
		
		for(u32 tau = std::max(0, (s32)(t-L+1)); tau <= t ; tau++)
		{
	   		for(u32 i=0 ; i < trajs[tau].size() ; i++)
			{
				cv::Point p_t = trajs[tau][i].vecPoint.back();
				cv::Point p_tn;
				p_tn.x = std::max(0, std::min((int)(p_t.x + maU(p_t.x, p_t.y)), flowMedX.cols-1 ));
				p_tn.y = std::max(0, std::min((int)(p_t.y + maV(p_t.x, p_t.y)), flowMedX.rows-1 ));
				trajs[tau][i].vecPoint.push_back(p_tn);
			}
		}
	}
	std::cout << "End DenseTraj::TrackTraj()" << std::endl;  // DEBUG
}

void DenseTraj::PruneTraj(Trajectories& trajs)
{
	std::cout << "Start DenseTraj::PruneTraj()" << std::endl;  // DEBUG
	for (u32 t = 0; t < trajs.size(); t++) 
	{
		for(u32 i=0 ; i < trajs[t].size() ; i++)
		{
			f32 d = 0;
			for(u32 j=0 ; j < trajs[t][i].vecPoint.size()-1 ; j++)
			{
				d += cv::norm(trajs[t][i].vecPoint[j] - trajs[t][i].vecPoint[j+1]);
			}
			if(d == 0)
			{
				trajs[t].erase(trajs[t].begin()+i);
				i--;
				continue;
			}
           
			for(u32 j=0 ; j < trajs[t][i].vecPoint.size()-1 ; j++)
			{
				if(cv::norm(trajs[t][i].vecPoint[j] - trajs[t][i].vecPoint[j+1]) > 0.7 * d)
				{
					trajs[t].erase(trajs[t].begin()+i);
					i--;
					break;
				}
			
			}
         
		}
	}
	std::cout << "End DenseTraj::PruneTraj()" << std::endl;  // DEBUG
}

void DenseTraj::GetDescripter(const Video& video, const Trajectory& traj,
		const Video& gradAngle, const Video& gradMag,
    	const Video& flowAngle, const Video& flowMag, 
    	const Video& LxFAngle, const Video& LxFMag,
    	const Video& LyFAngle, const Video& LyFMag,
    	Math::Vector<Float>& descriptor, u32 N, u32 L, u32 ns, u32 nt)	
{
//	std::cout << "Start DenseTraj::GetDescripter()" << std::endl;  // DEBUG
//    std::cout << ">> generate trajectory descripter" << std::endl;
	Math::Vector<Float> trjdescript(2*L);
	f32 d = 0;
	for(u32 i = 0 ; i < traj.vecPoint.size()-1 ; i++)
	{
		cv::Point diff = traj.vecPoint[i+1] - traj.vecPoint[i];
		trjdescript.at(2*i)   = diff.x;
		trjdescript.at(2*i+1) = diff.y;
		d += cv::norm(diff);
	}
	trjdescript.scale(1.0 / d);	
    
//    std::cout << ">> generate histograms" << std::endl;

	std::vector< Math::Vector<Float> >  histHog;
	std::vector< Math::Vector<Float> >  histHof;
	std::vector< Math::Vector<Float> >  histMbhX;
	std::vector< Math::Vector<Float> >  histMbhY;

	u32 nGrid = ns * ns * nt;
	for (u32 h = 0; h < nGrid; h++) { // 3x3x2 hog and hof histograms
		histHog.push_back(Math::Vector<Float>(8)); // use four bins per histogram
		histHog.back().setToZero();
		histHof.push_back(Math::Vector<Float>(9)); // use four bins per histogram
		histHof.back().setToZero();
		histMbhX.push_back(Math::Vector<Float>(8)); // use four bins per histogram
		histMbhX.back().setToZero();
		histMbhY.push_back(Math::Vector<Float>(8)); // use four bins per histogram
		histMbhY.back().setToZero();
	}

	for(u32 i = 0 ; i <= traj.vecPoint.size() ; i++)
	{
        u32 t = traj.tStart + i;

        if(t >= video.size()-1) break;    // optical flows have one less indices        
		MemoryAccessor angleHog(gradAngle.at(t).rows, gradAngle.at(t).cols, (Float*)gradAngle.at(t).data);
		MemoryAccessor magHog(gradMag.at(t).rows, gradMag.at(t).cols, (Float*)gradMag.at(t).data);
        MemoryAccessor angleHof(flowAngle.at(t).rows, flowAngle.at(t).cols, (Float*)flowAngle.at(t).data);
		MemoryAccessor magHof(flowMag.at(t).rows, flowMag.at(t).cols, (Float*)flowMag.at(t).data);
	    MemoryAccessor angleMbhX(LxFAngle.at(t).rows, LxFAngle.at(t).cols, (Float*)LxFAngle.at(t).data);
		MemoryAccessor magMbhX(LxFMag.at(t).rows, LxFMag.at(t).cols, (Float*)LxFMag.at(t).data);
	    MemoryAccessor angleMbhY(LyFAngle.at(t).rows, LyFAngle.at(t).cols, (Float*)LyFAngle.at(t).data);
        MemoryAccessor magMbhY(LyFMag.at(t).rows, LyFMag.at(t).cols, (Float*)LyFMag.at(t).data);
		for (u32 x = std::max(0, (s32)(traj.vecPoint[i].x - N/2) )
			; x < std::min((u32)gradAngle.at(t).cols, (u32)(traj.vecPoint[i].x + N/2) ) ; x++) 
        {
			for (u32 y = std::max(0, (s32)(traj.vecPoint[i].y - N/2) )
				; y < std::min( (u32)gradAngle.at(t).rows, (u32)(traj.vecPoint[i].y + N/2) ) ; y++) 
            {
				// compute histogram index of current pixel (x,y,t) in the 3x3x2 grid
				u32 histIdx = 0;
				if (i > L/2) histIdx += 9;
				if (x > (Float)traj.vecPoint[i].x - (1.0 / 3.0)*N/2) histIdx += 3;
				if (x > (Float)traj.vecPoint[i].x + (1.0 / 3.0)*N/2) histIdx += 3;
				if (y > (Float)traj.vecPoint[i].y - (1.0 / 3.0)*N/2) histIdx += 1;
				if (y > (Float)traj.vecPoint[i].y + (1.0 / 3.0)*N/2) histIdx += 1;
				// compute bin index in 144-dimensional array (3*3*2*4*2)
				u32 binIdx = u32(angleHog(x,y) / 45.1) % 8; // 45° leads to 8 bins, % 4: mirror to make sign independent
				histHog[histIdx].at(binIdx) += magHog(x,y);
				binIdx = u32(angleHof(x,y) / 40.1) % 9; // 45° leads to 8 bins, % 4: mirror to make sign independent
				histHof[histIdx].at(binIdx) += magHof(x,y);
				binIdx = u32(angleMbhX(x,y) / 45.1) % 8; // 45° leads to 8 bins, % 4: mirror to make sign independent
				histMbhX[histIdx].at(binIdx) += magMbhX(x,y);
				binIdx = u32(angleMbhY(x,y) / 45.1) % 8; // 45° leads to 8 bins, % 4: mirror to make sign independent
				histMbhY[histIdx].at(binIdx) += magMbhY(x,y);
			}
		}
	}

//    std::cout << ">> normalize all histograms" << std::endl;
	// normalize all histograms
	for (u32 h = 0; h < histHog.size(); h++) 
    {
		if (histHog.at(h).asum() > 0)
			histHog.at(h).scale(1.0 / histHog.at(h).asum());
		if (histHof.at(h).asum() > 0)
			histHof.at(h).scale(1.0 / histHof.at(h).asum());
		if (histMbhX.at(h).normEuclidean() > 0)
			histMbhX.at(h).scale(1.0 / histMbhX.at(h).normEuclidean());
		if (histMbhY.at(h).normEuclidean() > 0)
			histMbhY.at(h).scale(1.0 / histMbhY.at(h).normEuclidean());
	}

//    std::cout << ">> combine everything in result vector" << std::endl;
	// combine everything in result vector
	descriptor.resize(2*L + nGrid*(8+9+8+8) );
	for(u32 i = 0 ; i < 2*L ; i++)
	{
		descriptor.at(i) = trjdescript.at(i);
	}

	for (u32 h = 0; h < histHog.size(); h++) {
		for (u32 bin = 0; bin < 8; bin++) {
			descriptor.at(2*L               + h*8 + bin) = histHog.at(h).at(bin);			
		}
		for (u32 bin = 0; bin < 9; bin++) {
			descriptor.at(2*L+nGrid*8       + h*9 + bin) = histHof.at(h).at(bin);			
		}
		for (u32 bin = 0; bin < 8; bin++) {
			descriptor.at(2*L+nGrid*(8+9)   + h*8 + bin) = histMbhX.at(h).at(bin);			
		}
		for (u32 bin = 0; bin < 8; bin++) {
			descriptor.at(2*L+nGrid*(8+9+8) + h*8 + bin) = histMbhY.at(h).at(bin);			
		}		
	}

//    std::cout << "End DenseTraj::GetDescripter()" << std::endl;  // DEBUG	
}


void DenseTraj::ShowWithDT(const Video& video, const Trajectories& trajs, u32 L)
{

	Video vidTmp = video;
	for (u32 t = 0; t < trajs.size(); t++) 
	{
		
        for(u32 tau = std::max(0, (s32)(t-L+1)); tau <= t ; tau++)
		{
			for(u32 i=0 ; i < trajs[tau].size() ; i++)
			{
				cv::Point ptTmp = trajs[tau][i].vecPoint[0];
				
                for(u32 j=1 ; j < std::min( (u32)trajs[tau][i].vecPoint.size() , (u32)(t - trajs[tau][i].tStart) ) ; j++)
				{
				    cv::Point p1(ptTmp.y, ptTmp.x);
				    cv::Point p2(trajs[tau][i].vecPoint[j].y, trajs[tau][i].vecPoint[j].x);
				    if(vidTmp[t].channels() == 3)
						cv::line(vidTmp[t], p1, p2, cv::Scalar(0,255,0));
					else
						cv::line(vidTmp[t], p1, p2, 0.5);
					ptTmp = trajs[tau][i].vecPoint[j];
				}
                
				cv::Point p1(ptTmp.y-1, ptTmp.x-1);
				cv::Point p2(ptTmp.y+1, ptTmp.x+1);
				if(vidTmp[t].channels() == 3)
		            cv::rectangle(vidTmp[t], p1, p2, cv::Scalar(0,0,255), 1);
		        else
		            cv::rectangle(vidTmp[t], p1, p2, 0.5, 1);
		        
			}
		}
	}
	for (u32 t = 0; t < video.size(); t++) {
		cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE );
		cv::imshow("Display window", vidTmp.at(t));
		if(cv::waitKey(30) >= 0) break;
	}
    cv::waitKey(0);	
   
}

void DenseTraj::ReadVideo(const std::string& filename, Video& result, Video& raw) {

	// open video file
	cv::VideoCapture capture(filename);
	if(!capture.isOpened())
		Core::Error::msg() << "Unable to open Video: " << filename << Core::Error::abort;
	cv::Mat frame, tmp;
	result.clear();
    raw.clear();
	// read all frames
	u32 nFrames = capture.get(CV_CAP_PROP_FRAME_COUNT);
	while ((nFrames > 0) && (capture.read(frame))) {
        raw.push_back(frame.clone());
		if (frame.channels() == 3)
			cv::cvtColor(frame, tmp, CV_BGR2GRAY);
		else
			tmp = frame;
		result.push_back(cv::Mat());
		tmp.convertTo(result.back(), CV_32FC1, 1.0/255.0);
		nFrames--;
	}
	capture.release();
}

void DenseTraj::Derivatives2D(const Video& in, Video& Lx, Video& Ly)
{
	Lx.resize(in.size());
	Ly.resize(in.size());
	/* loop over the original frames */
	for (u32 t = 0; t < in.size(); t++) {
		cv::Sobel(in.at(t), Lx.at(t), CV_32FC1, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
		cv::Sobel(in.at(t), Ly.at(t), CV_32FC1, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);
	}
}

void DenseTraj::Derivatives3D(const Video& in, Video& Lx, Video& Ly, Video& Lt)
{
	Lx.resize(in.size());
	Ly.resize(in.size());
	Lt.resize(in.size() - 1);
	/* loop over the original frames */
	for (u32 t = 0; t < in.size(); t++) {
		cv::Sobel(in.at(t), Lx.at(t), CV_32FC1, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
		cv::Sobel(in.at(t), Ly.at(t), CV_32FC1, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);
	}
	for (u32 t = 0; t < in.size() - 1; t++) {
		Lt.at(t) = in.at(t) - in.at(t+1);
	}
}

void DenseTraj::ConvertCartToPolar(const Video& x, const Video& y, Video& angle, Video& mag)
{
	angle.clear();
	mag.clear();
	for (u32 t = 0; t < x.size(); t++) {
		cv::Mat imgMag, imgAngle;
		cv::cartToPolar(x.at(t), y.at(t), imgMag, imgAngle, true);
		angle.push_back(imgAngle);
		mag.push_back(imgMag);
	}
}


void DenseTraj::OpticalFlow(const Video& video, Video& flowX, Video& flowY)
{
	flowX.clear();
	flowY.clear();
	for (u32 t = 0; t < video.size() - 1; t++)
    {
		cv::Mat tmpFlow;
		cv::Mat tmpXY[2];
		cv::calcOpticalFlowFarneback(video.at(t), video.at(t+1), tmpFlow, 0.702, 5, 10, 2, 7, 1.5, cv::OPTFLOW_FARNEBACK_GAUSSIAN );
		cv::split(tmpFlow, tmpXY);
		flowX.push_back(tmpXY[0]);
		flowY.push_back(tmpXY[1]);
	}
}

void DenseTraj::filter3d(const Video& in, Video& out, Float sigma, Float tau) {
	out.clear();
	// filter in spatial domain
	Video tmp(in.size());
	for (u32 t = 0; t < in.size(); t++) {
		cv::GaussianBlur(in.at(t), tmp.at(t), cv::Size(0,0), sigma, sigma);
	}
	// filter in temporal domain
	u32 ksize = u32(3.0 * tau);
	cv::Mat kernel = cv::getGaussianKernel(ksize*2 + 1, tau, CV_32F);
	for (u32 t = 0; t < in.size(); t++) {
		out.push_back(cv::Mat(tmp.at(t).rows, tmp.at(t).cols, tmp.at(t).type(), 0.0));
		Float* ptr1 = (Float*) out.back().data;
		// add weighted neighboring frames
		for (s32 w = -ksize; w <= (s32)ksize; w++) {
			u32 i = std::min(std::max(0, (s32)t+w), (s32)tmp.size() - 1);
			Float* ptr2 = (Float*) tmp.at(i).data;
			Math::axpy(tmp.at(t).rows * tmp.at(t).cols, kernel.at<Float>(w+ksize), ptr2, 1, ptr1, 1);
		}
	}
}


