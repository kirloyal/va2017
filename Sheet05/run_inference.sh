#!/bin/bash
    
mkdir -p log results/inference

for SEQUENCE in s1 s2 s3
do
    ### EVALUATE RESULT OF THIS ITERATION ON THE TEST SET ###
    OPTIONS="--log-file=log/inference_$SEQUENCE.log \
             --grammar.file=data/breakfast_toy.grammar_${SEQUENCE}.gz \
             --features.label-writer.feature-cache=results/inference/$SEQUENCE.labels"
    ./src/Hmm/hmm-tool --config=config/decode.config $OPTIONS
done
