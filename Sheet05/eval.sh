#!/bin/bash

GROUND_TRUTH=$1
RECOGNIZED=$2

OPTIONS="--log-file=/dev/null \
         --action=evaluation \
         --reader_gt.feature-cache=$GROUND_TRUTH \
         --reader_res.feature-cache=$RECOGNIZED"

./src/Hmm/hmm-tool $OPTIONS
