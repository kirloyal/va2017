/*
 * Scorer.hh
 *
 *  Created on: May 12, 2017
 *      Author: richard
 */

#ifndef GMM_TRAINER_HH_
#define GMM_TRAINER_HH_

#include <Features/FeatureReader.hh>
#include <Features/FeatureWriter.hh>
#include <Core/CommonHeaders.hh>
#include <Math/Matrix.hh>
#include <Math/Vector.hh>

namespace Hmm {

class GMMTrainer
{
private:
	static const Core::ParameterString paramMeanFile_;
	static const Core::ParameterString paramVarFile_;
protected:
	u32 nStates_;
	u32 nDims_;
	std::string meanFile_;
	std::string varFile_;
	Math::Matrix<Float> mean_;
	Math::Matrix<Float> var_;
	Math::Vector<Float> gConst_;
	Math::Matrix<Float> scores_;
public:
	GMMTrainer();
	virtual ~GMMTrainer() {}
	virtual void applySequence( Features::SequenceFeatureReader& reader,  Features::SequenceLabelReader& labelReader);
};

} // namespace

#endif /* HMM_SCORER_HH_ */
