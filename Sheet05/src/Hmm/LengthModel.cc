/*
 * LengthModel.cc
 *
 *  Created on: May 17, 2017
 *      Author: richard
 */

#include "LengthModel.hh"

using namespace Hmm;

const Core::ParameterEnum LengthModel::paramLengthModelType_("type", "none, poisson", "none", "length-model");

LengthModel::LengthModel() :
		type_((LengthModelType) Core::Configuration::config(paramLengthModelType_))
{}

Float LengthModel::score(u32 length, u32 state) {
	return 0.0;
}

LengthModel* LengthModel::create() {
	switch ((LengthModelType) Core::Configuration::config(paramLengthModelType_)) {
	case none:
		return new LengthModel();
		break;
	case poisson:
		return new PoissonLengthModel();
		break;
	default:
		return 0; // this can not happen
	}
}

/*
 * PoissonLengthModel
 */
const Core::ParameterString PoissonLengthModel::paramLengthModelFile_("model-file", "", "length-model");

// if true re-scale probabilities such that maximal probability (at lambda) is 1
const Core::ParameterBool PoissonLengthModel::paramRescale_("re-scale", false, "length-model");

PoissonLengthModel::PoissonLengthModel() :
		Precursor(),
		lengthModelFile_(Core::Configuration::config(paramLengthModelFile_)),
		rescale_(Core::Configuration::config(paramRescale_)),
		isInitialized_(false)
{}

void PoissonLengthModel::initialize() {
	if (lengthModelFile_.empty())
		Core::Error::msg("PoissonLengthModel::initialize(): model-file not specified.") << Core::Error::abort;
	lambda_.read(lengthModelFile_);
	rescalingFactor_.resize(lambda_.size());
	rescalingFactor_.setToZero();
	if (rescale_) {
		for (u32 state = 0; state < lambda_.size(); state++) {
			Float logFak = 0;
			for (u32 k = 2; k <= lambda_.at(state); k++)
				logFak += std::log(k);
			rescalingFactor_.at(state) = round(lambda_.at(state)) * std::log(round(lambda_.at(state))) - round(lambda_.at(state)) - logFak;
		}
	}
	isInitialized_ = true;
}

Float PoissonLengthModel::score(u32 length, u32 state) {
	require(isInitialized_);
	require_lt(state, lambda_.size());
	Float logFak = 0;
	for (u32 k = 2; k <= length; k++)
		logFak += std::log(k);
	return length * std::log(lambda_.at(state)) - lambda_.at(state) - logFak - rescalingFactor_.at(state);
}
