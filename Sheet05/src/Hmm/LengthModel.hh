/*
 * LengthModel.hh
 *
 *  Created on: May 17, 2017
 *      Author: richard
 */

#ifndef HMM_LENGTHMODEL_HH_
#define HMM_LENGTHMODEL_HH_

#include <Core/CommonHeaders.hh>
#include <Math/Vector.hh>
#include <Math/Matrix.hh>

namespace Hmm {

class LengthModel
{
private:
	static const Core::ParameterEnum paramLengthModelType_;
public:
	enum LengthModelType { none, poisson };
private:
	LengthModelType type_;
public:
	LengthModel();
	virtual ~LengthModel() {}
	LengthModelType type() { return type_; }
	virtual void initialize() {}
	virtual Float score(u32 length, u32 state);

	/*
	 * factory
	 */
	static LengthModel* create();
};

/*
 * Poisson length model
 */
class PoissonLengthModel : public LengthModel
{
private:
	static const Core::ParameterString paramLengthModelFile_;
	static const Core::ParameterBool paramRescale_;
	typedef LengthModel Precursor;
	std::string lengthModelFile_;
	bool rescale_;
	Math::Vector<Float> lambda_;
	Math::Vector<Float> rescalingFactor_;
	bool isInitialized_;
public:
	PoissonLengthModel();
	virtual ~PoissonLengthModel() {}
	virtual void initialize();
	virtual Float score(u32 length, u32 state);
};

} // namespace

#endif /* HMM_LENGTHMODEL_HH_ */
