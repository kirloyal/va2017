/*
 * TransitionProbabilityEstimator.cc
 *
 *  Created on: Jun 30, 2017
 *      Author: richard
 */

#include "TransitionProbabilityEstimator.hh"
#include <Math/Vector.hh>

using namespace Hmm;

const Core::ParameterString TransitionProbabilityEstimator::paramTransitionProbabilityFile_("file", "", "transition-probability-estimator");

TransitionProbabilityEstimator::TransitionProbabilityEstimator() :
		file_(Core::Configuration::config(paramTransitionProbabilityFile_))
{
	if (file_.empty())
		Core::Error::msg("Hmm::TransitionProbabilityEstimator: no output file given.") << Core::Error::abort;
}

void TransitionProbabilityEstimator::estimate() {
	labelReader_.initialize();
	Math::Vector<Float> labelCount(labelReader_.featureDimension());
	labelCount.fill(0);
	Math::Vector<Float> loopCount(labelReader_.featureDimension());
	loopCount.fill(0);

	while (labelReader_.hasSequences()) {
		const std::vector<u32> labels = labelReader_.nextLabelSequence();
		for (u32 c = 0; c < labels.size(); c++) {
			labelCount.at(labels.at(c))++;
			if ((c > 0) && (labels.at(c-1) == labels.at(c)))
				loopCount.at(labels.at(c))++;
		}
	}

	labelCount.ensureMinimalValue(1);
	loopCount.elementwiseDivision(labelCount);

	loopCount.write(file_);
}
