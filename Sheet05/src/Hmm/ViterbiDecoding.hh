/*
 * ViterbiDecoding.hh
 *
 *  Created on: May 10, 2017
 *      Author: richard
 */

#ifndef HMM_VITERBIDECODING_HH_
#define HMM_VITERBIDECODING_HH_

#include <Core/CommonHeaders.hh>
#include <Math/Vector.hh>
#include <Math/Matrix.hh>
#include "Grammar.hh"
#include "Scorer.hh"
#include "LengthModel.hh"
#include "HiddenMarkovModel.hh"

namespace Hmm {

class ViterbiDecoding
{
public:
	struct ActionSegment {
		u32 label;
		u32 length;
		ActionSegment(u32 _label, u32 _length) : label(_label), length(_length) {}
	};

private:
	/* construct linked list as bookkeeping for hypotheses */

	/* traceback node */
	class TracebackNode {
	public:
		TracebackNode* predecessor;
		u32 state;
		u32 nSuccessors;
		bool isActionBoundary;
		TracebackNode(u32 _state, TracebackNode* _predecessor);
		~TracebackNode();
	};

	/* a node in the list */
	class HypothesisNode {
	public:
		u32 context;
		u32 state;
		u32 length;
		Float score;
		TracebackNode* traceback;
		HypothesisNode* predecessor;
		HypothesisNode* successor;
		HypothesisNode(u32 _context, u32 _state, u32 _length = 1);
		~HypothesisNode();
		void update(Float score, TracebackNode* predecessorTraceback, bool isActionBoundary = false);
	};

	/* the list */
	class HypothesisList {
	private:
		HypothesisNode *head_;
		std::vector< HypothesisNode* > contextPointer_;
		u32 nHypotheses_;
		HypothesisNode& insert(u32 context, u32 state, u32 length = 1);
		void remove(HypothesisNode* node);
	public:
		HypothesisList(u32 nContexts);
		~HypothesisList();
		u32 nHypotheses() const { return nHypotheses_; }
		void clear();
		HypothesisNode& get(u32 context, u32 state, u32 length = 1);
		HypothesisNode& head();
		void prune(Float pruningThreshold);
		void swap(HypothesisList& h);
	};

	/* the class for the actual Viterbi decoding */
private:
	static const Core::ParameterEnum paramViterbiOutput_;
	static const Core::ParameterFloat paramGrammarScale_;
	static const Core::ParameterFloat paramLengthModelScale_;
	static const Core::ParameterInt paramMaximalLength_;
	static const Core::ParameterFloat paramPruningThreshold_;
	enum ViterbiOutput { hmmStates, labels };
protected:
	ViterbiOutput outputType_;
	Float grammarScale_;
	Float lengthModelScale_;
	u32 maxLength_;
	Float pruningThreshold_;
	Grammar* grammar_;
	Scorer* scorer_;
	LengthModel* lengthModel_;
	HiddenMarkovModel* hmm_;
	std::vector<ActionSegment> segmentation_;
	std::vector<u32> framewiseRecognition_;
	bool isInitialized_;
private:
	void decodeFrame(u32 t, HypothesisList& oldHyp, HypothesisList& newHyp);
	void traceback(HypothesisList& hyp, u32 sequenceLength);
public:
	ViterbiDecoding();
	virtual ~ViterbiDecoding();
	void initialize();
	void sanityCheck();
	Float decode(const Math::Matrix<Float>& sequence);
	Float decodePath(const Math::Matrix<Float>& sequence, const std::vector<unsigned int>& path);
	Float realign(const Math::Matrix<Float>& sequence, const std::vector<u32>& labelSequence);
	const std::vector<ActionSegment>& segmentation() const { return segmentation_; }
	const std::vector<u32>& framewiseRecognition() const { return framewiseRecognition_; }
	u32 nOutputClasses() const;
};

} // namespace

#endif /* HMM_VITERBIDECODING_HH_ */
