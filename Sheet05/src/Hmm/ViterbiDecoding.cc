/*
 * ViterbiDecoding.cc
 *
 *  Created on: May 10, 2017
 *      Author: richard
 */

#include "ViterbiDecoding.hh"

using namespace Hmm;

/*
 * TracebackNode
 */
ViterbiDecoding::TracebackNode::TracebackNode(u32 _state, TracebackNode* _predecessor) :
		predecessor(_predecessor),
		state(_state),
		nSuccessors(0),
		isActionBoundary(false)
{
	if (predecessor != 0)
		predecessor->nSuccessors++;
}

ViterbiDecoding::TracebackNode::~TracebackNode() {
	require_eq(nSuccessors, 0);
	if (predecessor != 0) {
		predecessor->nSuccessors--;
		if (predecessor->nSuccessors == 0)
			delete predecessor;
	}
}

/*
 * ViterbiDecoding::HypothesisNode
 */
ViterbiDecoding::HypothesisNode::HypothesisNode(u32 _context, u32 _state, u32 _length) :
		context(_context),
		state(_state),
		length(_length),
		score(-Types::inf<Float>()),
		traceback(new TracebackNode(state, 0)),
		predecessor(0),
		successor(0)
{}

ViterbiDecoding::HypothesisNode::~HypothesisNode() {
	if ((traceback != 0) && (traceback->nSuccessors == 0))
		delete traceback;
}

void ViterbiDecoding::HypothesisNode::update(Float _score, TracebackNode* predecessorTraceback, bool isActionBoundary) {
	if (_score > score) {
		score = _score;
		traceback->isActionBoundary = isActionBoundary;
		if (traceback->predecessor != 0)
			traceback->predecessor->nSuccessors--;
		traceback->predecessor = predecessorTraceback;
		if (traceback->predecessor != 0)
			traceback->predecessor->nSuccessors++;
	}
}

/*
 * ViterbiDecoding::HypothesisList
 */
ViterbiDecoding::HypothesisList::HypothesisList(u32 nContexts) :
		head_(0),
		contextPointer_(nContexts, 0),
		nHypotheses_(0)
{}

ViterbiDecoding::HypothesisList::~HypothesisList() {
	clear();
}

void ViterbiDecoding::HypothesisList::clear() {
	while (head_ != 0) {
		HypothesisNode *node = head_;
		head_ = head_->successor;
		delete node;
	}
	for (u32 i = 0; i < contextPointer_.size(); i++)
		contextPointer_.at(i) = 0;
	nHypotheses_ = 0;
}

ViterbiDecoding::HypothesisNode& ViterbiDecoding::HypothesisList::insert(u32 context, u32 state, u32 length) {
	HypothesisNode* node = new HypothesisNode(context, state, length);
	if (contextPointer_.at(context) == 0) {
		if (head_ != 0)
			head_->predecessor = node;
		node->successor = head_;
		head_ = node;
		contextPointer_.at(context) = head_;
	}
	else {
		node->predecessor = contextPointer_.at(context);
		node->successor = contextPointer_.at(context)->successor;
		if (contextPointer_.at(context)->successor != 0)
			contextPointer_.at(context)->successor->predecessor = node;
		contextPointer_.at(context)->successor = node;
	}
	nHypotheses_++;
	return *node;
}

void ViterbiDecoding::HypothesisList::remove(HypothesisNode* node) {
	if (contextPointer_.at(node->context) == node) {
		if ((node->successor != 0) && (node->successor->context == node->context))
			contextPointer_.at(node->context) = node->successor;
		else
			contextPointer_.at(node->context) = 0;
	}
	if (node->predecessor != 0)
		node->predecessor->successor = node->successor;
	if (node->successor != 0)
		node->successor->predecessor = node->predecessor;
	if (node == head_)
		head_ = node->successor;
	nHypotheses_--;
	delete node;
}

ViterbiDecoding::HypothesisNode& ViterbiDecoding::HypothesisList::get(u32 context, u32 state, u32 length) {
	HypothesisNode *node = contextPointer_.at(context);
	while ((node != 0) && (node->context == context) && ((node->state != state) || (node->length != length)))
		node = node->successor;
	if ((node != 0) && (node->context == context) && (node->state == state) && (node->length == length))
		return *node;
	else
		return insert(context, state, length);
}

ViterbiDecoding::HypothesisNode& ViterbiDecoding::HypothesisList::head() {
	return *head_;
}

void ViterbiDecoding::HypothesisList::prune(Float pruningThreshold) {
	if (pruningThreshold == Types::inf<Float>()) // no pruning if threshold is inf
		return;
	// find best score
	Float bestScore = -Types::inf<Float>();
	for (HypothesisNode* node = head_; node != 0; node = node->successor) {
		bestScore = (node->score > bestScore ? node->score : bestScore);
	}
	// prune bad hypotheses
	HypothesisNode* node = head_;
	while (node != 0) {
		HypothesisNode* tmp = node;
		node = node->successor;
		if (tmp->score + pruningThreshold < bestScore)
			remove(tmp);
	}
}

void ViterbiDecoding::HypothesisList::swap(HypothesisList& h) {
	std::swap(h.head_, head_);
	std::swap(h.nHypotheses_, nHypotheses_);
	contextPointer_.swap(h.contextPointer_);
}

/*
 * ViterbiDecoding
 */
const Core::ParameterEnum ViterbiDecoding::paramViterbiOutput_("output", "hmm-states, labels", "labels", "viterbi-decoding");

const Core::ParameterFloat ViterbiDecoding::paramGrammarScale_("grammar-scale", 1.0, "viterbi-decoding");

const Core::ParameterFloat ViterbiDecoding::paramLengthModelScale_("length-model-scale", 1.0, "viterbi-decoding");

const Core::ParameterInt ViterbiDecoding::paramMaximalLength_("maximal-length", Types::max<u32>(), "viterbi-decoding");

const Core::ParameterFloat ViterbiDecoding::paramPruningThreshold_("pruning-threshold", Types::inf<Float>(), "viterbi-decoding");

ViterbiDecoding::ViterbiDecoding() :
		outputType_((ViterbiOutput) Core::Configuration::config(paramViterbiOutput_)),
		grammarScale_(Core::Configuration::config(paramGrammarScale_)),
		lengthModelScale_(Core::Configuration::config(paramLengthModelScale_)),
		maxLength_(Core::Configuration::config(paramMaximalLength_)),
		pruningThreshold_(Core::Configuration::config(paramPruningThreshold_)),
		grammar_(0),
		scorer_(0),
		lengthModel_(0),
		hmm_(0),
		isInitialized_(false)
{
	if (pruningThreshold_ < 0.0)
		Core::Error::msg("ViterbiDecoding: pruning-threshold needs to be non-negative.") << Core::Error::abort;
}

ViterbiDecoding::~ViterbiDecoding() {
	delete grammar_;
	delete scorer_;
	delete lengthModel_;
	delete hmm_;
}

void ViterbiDecoding::initialize() {
	grammar_ = Grammar::create();
	grammar_->initialize();
	scorer_ = Scorer::create();
	scorer_->initialize();
	lengthModel_ = LengthModel::create();
	lengthModel_->initialize();
	hmm_ = HiddenMarkovModel::create();
	hmm_->initialize();
	isInitialized_ = true;
}

void ViterbiDecoding::sanityCheck() {
	// check number of hmm states
	if (hmm_->nStates() != scorer_->nClasses())
		Core::Error::msg("ViterbiDecoding::initialize: hmm state mismatch (") <<  hmm_->nStates() << " in hidden Markov model vs. "
		<< scorer_->nClasses() << " in scorer)." << Core::Error::abort;
	// grammar must have at most as many terminal symbols as there are classes (plus one sentence end symbol)
	if (hmm_->nClasses() + 1 < grammar_->nTerminals())
		Core::Error::msg("ViterbiDecoding::initialize: class mismatch (") << hmm_->nClasses() << " in hidden Markov model vs. "
		<< grammar_->nTerminals()-1 << " in grammar)." << Core::Error::abort;
}

void ViterbiDecoding::decodeFrame(u32 t, HypothesisList& oldHyp, HypothesisList& newHyp) {
	// for each hypothesis in oldHyp create the valid new hypotheses
	for (HypothesisNode* node = &(oldHyp.head()); node != 0; node = node->successor) {
		/* loop transition (stay in the same hmm state) */
		Float score = node->score + hmm_->transitionScore(node->state, node->state) + scorer_->frameScore(t, node->state);
		if (lengthModel_->type() == LengthModel::none)
			newHyp.get(node->context, node->state).update(score, node->traceback);
		else if (node->length < maxLength_)
			newHyp.get(node->context, node->state, node->length + 1).update(score, node->traceback);
		/* hmm end state: hypothesize action end and start new hypothesis using grammar */
		if (hmm_->isEndState(node->state)) {
			// ... start a new class hypothesis for each transition allowed by the grammar
			const std::vector<Grammar::Rule>& rules = grammar_->rules(node->context);
			for (u32 rule = 0; rule < rules.size(); rule++) {
				// exclude transitions to sequence end symbol as long as we are not at the end of the sequence
				if (rules.at(rule).context == grammar_->endSymbol())
					continue;
				u32 state = hmm_->startState(rules.at(rule).label);
				score = node->score + lengthModelScale_ * lengthModel_->score(node->length, node->state)
						+ scorer_->segmentScore(t-1, node->length, node->state)
						+ grammarScale_ * rules.at(rule).logProbability
						+ hmm_->transitionScore(node->state, state) + scorer_->frameScore(t, state);
				newHyp.get(rules.at(rule).context, state).update(score, node->traceback, true);
			}
		}
		/* no hmm end state: forward transition (loop already treated above) */
		else {
			// transition to next state
			u32 state = hmm_->successor(node->state);
			score = node->score + lengthModelScale_ * lengthModel_->score(node->length, node->state)
					+ scorer_->segmentScore(t-1, node->length, node->state)
					+ hmm_->transitionScore(node->state, state) + scorer_->frameScore(t, state);
			newHyp.get(node->context, state).update(score, node->traceback);
		}
	}
}

void ViterbiDecoding::traceback(HypothesisList& hyp, u32 sequenceLength) {
	// check if some valid path could be found
	segmentation_.clear();
	framewiseRecognition_.clear();
	if (hyp.nHypotheses() == 0) {
		Core::Log::os("No valid hypothesis found. Either sequence is too short or pruning is too strong.");
		segmentation_.push_back(ActionSegment(0, sequenceLength));
		framewiseRecognition_.resize(sequenceLength, 0);
	}
	// reconstruct decoded state- and label sequence
	else {
		// trace back best sequence
		u32 length = 0;
		for (TracebackNode* traceback = hyp.head().traceback->predecessor; traceback != 0; traceback = traceback->predecessor) {
			length++;
			if (outputType_ == hmmStates) {
				framewiseRecognition_.insert(framewiseRecognition_.begin(), traceback->state);
				if ((framewiseRecognition_.size() > 1) && (framewiseRecognition_[0] == framewiseRecognition_[1]))
					segmentation_[0].length++;
				else
					segmentation_.insert(segmentation_.begin(), ActionSegment(traceback->state, 1));
			}
			else { // outputType_ == labels
				framewiseRecognition_.insert(framewiseRecognition_.begin(), hmm_->getClass(traceback->state));
				if (traceback->isActionBoundary) {
					segmentation_.insert(segmentation_.begin(), ActionSegment(hmm_->getClass(traceback->state), length));
					length = 0;
				}
			}
		}
	}
}

Float ViterbiDecoding::decode(const Math::Matrix<Float>& sequence) {
	require(isInitialized_);
	u32 T = sequence.nColumns();

	scorer_->setSequence(sequence);
	HypothesisList oldHyp(grammar_->nNonterminals());
	HypothesisList newHyp(grammar_->nNonterminals());

	// create initial hypotheses
	const std::vector<Grammar::Rule>& rules = grammar_->rules(grammar_->startSymbol());
	for (u32 rule = 0; rule < rules.size(); rule++) {
		u32 state = hmm_->startState(rules.at(rule).label);
		Float score = grammarScale_ * rules.at(rule).logProbability + scorer_->frameScore(0, state);
		oldHyp.get(rules.at(rule).context, state).update(score, 0, true);
	}

	// decode all remaining frames
	for (u32 t = 1; t < T; t++) {
		// viterbi decoding of frame t
		decodeFrame(t, oldHyp, newHyp);
		// oldHyp must me cleared before nodes in newHyp are deleted due to traceback pointers!
		oldHyp.clear();
		// prune
		newHyp.prune(pruningThreshold_);
		// swap old and new hypotheses for processing of next frame
		oldHyp.swap(newHyp);
	}

	// find best hypothesis (among all hypotheses that allow a transition to the sequence end symbol)
	for (HypothesisNode* node = &(oldHyp.head()); node != 0; node = node->successor) {
		if (hmm_->isEndState(node->state)) {
			const std::vector<Grammar::Rule>& rules = grammar_->rules(node->context);
			for (u32 rule = 0; rule < rules.size(); rule++) {
				if (rules.at(rule).context == grammar_->endSymbol()) {
					// hmm transition probability: node->state+1 may not exist but does not matter since node->state in an end-state
					newHyp.get(grammar_->endSymbol(), Types::max<u32>()).update(
							node->score + grammarScale_ * rules.at(rule).logProbability + hmm_->transitionScore(node->state, node->state+1)
							+ lengthModelScale_ * lengthModel_->score(node->length, node->state)
							+ scorer_->segmentScore(T-1, node->length, node->state),
							node->traceback);
				}
			}
		}
	}

	Float score = (newHyp.nHypotheses() == 0) ? -Types::inf<Float>() : newHyp.head().score;
	traceback(newHyp, T);

	// clean up
	oldHyp.clear(); // oldHyp needs to be cleared before newHyp due to traceback pointers
	newHyp.clear();

	return score;
}

Float ViterbiDecoding::realign(const Math::Matrix<Float>& sequence, const std::vector<u32>& labelSequence) {
	require(isInitialized_);
	if (grammar_->type() != Grammar::singlePath)
		Core::Error::msg("ViterbiDecoding::realign: grammar needs to be of type single-path for realignment.") << Core::Error::abort;
	dynamic_cast<SinglePathGrammar*>(grammar_)->setPath(labelSequence);
	sanityCheck();
	return decode(sequence);
}

u32 ViterbiDecoding::nOutputClasses() const {
	if (outputType_ == hmmStates)
		return hmm_->nStates();
	else // outputType_ == labels
		return hmm_->nClasses();
}
