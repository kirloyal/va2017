/*
 * Application.cc
 *
 *  Created on: Apr 10, 2014
 *      Author: richard
 */

#include "Application.hh"
#include "GrammarGenerator.hh"
#include "ViterbiDecoding.hh"
#include "GMMTrainer.hh"
#include "TransitionProbabilityEstimator.hh"
#include <Features/FeatureReader.hh>
#include <Features/FeatureWriter.hh>
#include <iostream>
#include <sstream>

using namespace Hmm;

APPLICATION(Hmm::Application)

const Core::ParameterEnum Application::paramAction_("action", "none, generate-grammar, inference, viterbi-decoding, realignment, train-hmm, evaluation, estimate-transition-probabilities", "none");

void Application::main() {

	switch (Core::Configuration::config(paramAction_)) {
	case generateGrammar:
		{
		GrammarGenerator* g = GrammarGenerator::create();
		g->generate();
		delete g;
		}
		break;
	case inference:
		infer();
		break;
	case viterbiDecoding:
		decode();
		break;
	case realignment:
		realign();
		break;
	case trainHMM:
		train();
		break;
	case evaluation:
		eval();
		break;
	case estimateTransitionProbabilities:
		trans();
		break;
	case none:
	default:
		Core::Error::msg("No action given.") << Core::Error::abort;
	}
}

void Application::infer() {

	ViterbiDecoding v;
	v.initialize();
	v.sanityCheck();

	Features::SequenceFeatureReader reader;
	reader.initialize();

	Features::SequenceLabelReader readerPath;
	readerPath.initialize();
	if (reader.totalNumberOfSequences() != readerPath.totalNumberOfSequences())
		Core::Error::msg("Application::infer: features.feature-reader and features.label-reader need to have the same number of sequence.") << Core::Error::abort;

	while (reader.hasSequences()) {

		Core::Log::openTag("sequence");
		while (readerPath.hasSequences()) {
			Float score = v.decode(reader.next());
			Core::Log::openTag("score");
			Core::Log::os() << score;
			Core::Log::closeTag();
			std::cout << "Score: " << score << std::endl;
		}
		Core::Log::closeTag();
		readerPath.newEpoch();

	}
}


void Application::decode() {

	ViterbiDecoding v;
	v.initialize();
	v.sanityCheck();

	Features::SequenceFeatureReader reader;
	reader.initialize();
	Features::SequenceLabelWriter labelWriter;
	labelWriter.initialize(reader.totalNumberOfFeatures(), v.nOutputClasses(), reader.totalNumberOfSequences());

	while (reader.hasSequences()) {
		Float score = v.decode(reader.next());
		Core::Log::openTag("sequence");
		Core::Log::openTag("score");
		Core::Log::os() << score;
		Core::Log::closeTag();
		Core::Log::openTag("recognized");
		std::stringstream s;
		for (u32 i = 0; i < v.segmentation().size(); i++)
			s << v.segmentation().at(i).label << ":" << v.segmentation().at(i).length << " ";
		Core::Log::os(s.str().c_str());
		Core::Log::closeTag();
		Core::Log::closeTag();
		labelWriter.write(v.framewiseRecognition());
		std::cout << "Score: " << score << std::endl;
	}

	labelWriter.finalize();
}

void Application::realign() {
	ViterbiDecoding v;
	v.initialize();

	Features::SequenceFeatureReader reader;
	Features::SequenceLabelReader labelReader;
	reader.initialize();
	labelReader.initialize();
	if (reader.totalNumberOfSequences() != labelReader.totalNumberOfSequences())
		Core::Error::msg("Application::realign: features.feature-reader and features.label-reader need to have the same number of sequence.") << Core::Error::abort;
	Features::SequenceLabelWriter labelWriter;
	labelWriter.initialize(reader.totalNumberOfFeatures(), v.nOutputClasses(), reader.totalNumberOfSequences());

	while (reader.hasSequences()) {
		Float score = v.realign(reader.next(), labelReader.nextLabelSequence());
		Core::Log::openTag("sequence");
		Core::Log::openTag("score");
		Core::Log::os() << score;
		Core::Log::closeTag();
		Core::Log::openTag("recognized");
		std::stringstream s;
		for (u32 i = 0; i < v.segmentation().size(); i++)
			s << v.segmentation().at(i).label << ":" << v.segmentation().at(i).length << " ";
		Core::Log::os(s.str().c_str());
		Core::Log::closeTag();
		Core::Log::closeTag();
		labelWriter.write(v.framewiseRecognition());
	}

	labelWriter.finalize();
}

void Application::train() {

	GMMTrainer gmmt;

	Features::SequenceFeatureReader reader;
	reader.initialize();

	Features::SequenceLabelReader labelReader;
	labelReader.initialize();

	if (reader.totalNumberOfSequences() != labelReader.totalNumberOfSequences())
		Core::Error::msg("Application::realign: features.feature-reader and features.label-reader need to have the same number of sequences.") << Core::Error::abort;


	gmmt.applySequence(reader, labelReader);

}

void Application::eval() {


	Features::SequenceLabelReader readerGT("reader_gt");
	readerGT.initialize();

	Features::SequenceLabelReader readerRes("reader_res");
	readerRes.initialize();

	if (readerGT.totalNumberOfSequences() != readerRes.totalNumberOfSequences())
		Core::Error::msg("Application::realign: readerGT and readerRes need to have the same number of sequence.") << Core::Error::abort;

	std::vector<unsigned int> vec_gt;
	std::vector<unsigned int> vec_res;

	Float frames_correct = 0;
	Float frames_all = 0;

	while (readerGT.hasSequences()) {

		vec_gt = readerGT.nextLabelSequence();
		vec_res = readerRes.nextLabelSequence();

		// collect all elements
		for (u32 colIdx = 0; colIdx < vec_gt.size() ; colIdx++) {

			if (vec_gt[colIdx] == vec_res[colIdx]){
				frames_correct++;
			}
			frames_all++;

		}
	}

	std::cout << " MoF " << frames_correct/frames_all << std::endl;

}

void Application::trans() {
	TransitionProbabilityEstimator tpe;
	tpe.estimate();
}
