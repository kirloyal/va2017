/*
 * FramewiseScorer.cc
 *
 *  Created on: May 12, 2017
 *      Author: richard
 */

#include "Scorer.hh"
#include <Nn/FeatureTransformation.hh>

using namespace Hmm;

/*
 * Scorer
 */
const Core::ParameterEnum Scorer::paramScorerType_("type", "framewise-neural-network-scorer, gmm-scorer",
		"gmm-scorer", "scorer");

Scorer::Scorer() :
		nClasses_(0),
		isInitialized_(false),
		sequence_(0)
{}

void Scorer::initialize() {
	isInitialized_ = true;
}

void Scorer::setSequence(const Math::Matrix<Float>& sequence) {
	require(isInitialized_);
	sequence_ = &sequence;
}

Scorer* Scorer::create() {
	switch ((ScorerType) Core::Configuration::config(paramScorerType_)) {
	case framewiseNeuralNetworkScorer:
		Core::Log::os("Create framewise-neural-network-scorer.");
		return new FramewiseNeuralNetworkScorer();
		break;
	case gmmScorer:
		Core::Log::os("Create GMM-scorer.");
		return new GaussianScorer();
		break;
	default:
		return 0; // this can not happen
	}
}


/*
 * FramewiseNeuralNetworkScorer
 */
const Core::ParameterString FramewiseNeuralNetworkScorer::paramPriorFile_("prior-file", "", "scorer");

const Core::ParameterFloat FramewiseNeuralNetworkScorer::paramPriorScale_("prior-scale", 1.0, "scorer");

const Core::ParameterBool FramewiseNeuralNetworkScorer::paramLogarithmizeNetworkOutput_("logarithmize-network-output", true, "scorer");

const Core::ParameterInt FramewiseNeuralNetworkScorer::paramBatchSize_("batch-size", 1, "scorer");

FramewiseNeuralNetworkScorer::FramewiseNeuralNetworkScorer() :
		Precursor(),
		priorFile_(Core::Configuration::config(paramPriorFile_)),
		priorScale_(Core::Configuration::config(paramPriorScale_)),
		logarithmizeNetworkOutput_(Core::Configuration::config(paramLogarithmizeNetworkOutput_)),
		batchSize_(Core::Configuration::config(paramBatchSize_))
{}

void FramewiseNeuralNetworkScorer::initialize() {
	Precursor::initialize();
	network_.initialize();
	nClasses_ = network_.outputDimension();
	prior_.resize(nClasses_);
	// initialize prior
	if (!priorFile_.empty()) {
		prior_.read(priorFile_);
		Core::Log::os("Load prior from ") << priorFile_;
		if (prior_.size() != nClasses_)
			Core::Error::msg("FramewiseNeuralNetworkScorer::initialized: prior-file must be a ") << nClasses_ << " dimensional vector." << Core::Error::abort;
	}
	prior_.initComputation();
	if (priorFile_.empty())
		prior_.fill(0.0);
	else
		prior_.log();
}

void FramewiseNeuralNetworkScorer::setSequence(const Math::Matrix<Float>& sequence) {
	scores_.initComputation();
	scores_.resize(network_.outputDimension(), sequence.nColumns());

	// process input sequence in several batches
	for (u32 colIdx = 0; colIdx < sequence.nColumns(); colIdx += batchSize_) {

		u32 batchSize = std::min(batchSize_, sequence.nColumns() - colIdx);
		Nn::Matrix batch(sequence.nRows(), batchSize);
		batch.copyBlockFromMatrix(sequence, 0, colIdx, 0, 0, sequence.nRows(), batchSize);

		Nn::FeatureTransformation featureTransformation(Nn::single);

		// if the feature transformation creates sequences out of the frames...
		if (featureTransformation.outputFormat() == Nn::sequence) {
			Nn::MatrixContainer input;
			featureTransformation.transform(batch, input);
			network_.setMaximalMemory(2);
			network_.forwardSequence(input, true);
		}
		// ... else simple work with the frames
		else {
			network_.forward(batch);
		}

		// store result in scores
		scores_.copyBlockFromMatrix(network_.outputLayer().latestActivations(0), 0, 0, 0, colIdx, network_.outputDimension(), batchSize);
	}

	if (logarithmizeNetworkOutput_)
		scores_.log();
	// subtract log prior
	scores_.addToAllColumns(prior_, -priorScale_);
	// make sure scores are all negative
	scores_.addConstantElementwise(-scores_.maxValue());
	scores_.finishComputation();
}

Float FramewiseNeuralNetworkScorer::frameScore(u32 t, u32 c) {
	require(isInitialized_);
	require_lt(c, scores_.nRows());
	require_lt(t, scores_.nColumns());
	return scores_.at(c, t);
}


/*
 * GaussianScorer
 */
const Core::ParameterString GaussianScorer::paramMeanFile_("mean-file", "", "scorer");

const Core::ParameterString GaussianScorer::paramVarFile_("var-file", "", "scorer");

GaussianScorer::GaussianScorer() :
		Precursor(),
		nDims_(0),
		meanFile_(Core::Configuration::config(paramMeanFile_)),
		varFile_(Core::Configuration::config(paramVarFile_))
{}

void GaussianScorer::initialize() {
	Precursor::initialize();

	// initialize mean
	if (!meanFile_.empty()) {
		mean_.read(meanFile_);
		Core::Log::os("Load mean from ") << meanFile_;
		nClasses_ = mean_.nRows();
		nDims_ = mean_.nColumns();
		if (nClasses_ == 0)
			Core::Error::msg("GaussianScorer::initialized: mean-file must be a ") << nClasses_ << " dimensional vector." << Core::Error::abort;
	}

	if (meanFile_.empty())
		mean_.fill(0.0);
//	else
//		mean_.log();

	// initialize var
	if (!varFile_.empty()) {
		var_.read(varFile_);
		Core::Log::os("Load var from ") << varFile_;
		if (var_.nRows() != nClasses_)
			Core::Error::msg("GaussianScorer::initialized: var-file must be a ") << nClasses_ << " dimensional vector." << Core::Error::abort;
	}

	if (varFile_.empty())
		var_.fill(1.0);
//	else
//		var_.log();

	gConst_.resize(mean_.nRows());
	gConst_.fill(0.0);

	for (u32 idx_const = 0; idx_const < gConst_.nRows(); idx_const++) {

		float gConstTmp = nDims_ * log(2* M_PI);

		Math::Vector<Float> var_vec;
		var_.getRow(idx_const, var_vec);

        for (u32 idx_covar = 0; idx_covar < var_vec.nRows(); idx_covar++) {
        	float val_tmp = var_vec.get(idx_covar);
        	gConstTmp +=  log(val_tmp) ;
        }

        gConst_.at(idx_const) = gConstTmp;

	}

	Core::Log::os("Scorer initialized! ") ;


}

void GaussianScorer::setSequence(const Math::Matrix<Float>& sequence) {

	scores_.resize(nClasses_ , sequence.nColumns());
	scores_.fill(0.0);

	Math::Vector<Float> feat_vec;
	Math::Vector<Float> mean_vec;
	Math::Vector<Float> var_vec;

	// process input sequence in several batches
	for (u32 colIdx = 0; colIdx < sequence.nColumns(); colIdx++) {

		sequence.getColumn(colIdx, feat_vec);

		for (u32 idx_const = 0; idx_const < gConst_.nRows(); idx_const++) {

			mean_.getRow(idx_const, mean_vec);
			var_.getRow(idx_const, var_vec);

			float sum_tmp = gConst_.get(idx_const);

	        for (u32 idx_dim = 0; idx_dim < feat_vec.nRows(); idx_dim++) {
	        	float feat_tmp = feat_vec.get(idx_dim);
	        	float mean_tmp = mean_vec.get(idx_dim);
	        	float var_tmp = var_vec.get(idx_dim);

	        	float xmm = feat_tmp - mean_tmp;

	        	sum_tmp +=  ( xmm * xmm * (1/var_tmp) );
	        }

	        sum_tmp *= -0.5;

	        scores_.at(idx_const, colIdx) = sum_tmp;

		}

	}



}

Float GaussianScorer::frameScore(u32 t, u32 c) {
	require(isInitialized_);
	require_lt(c, scores_.nRows());
	require_lt(t, scores_.nColumns());
	return scores_.at(c, t);
}
