/*
 * Scorer.hh
 *
 *  Created on: May 12, 2017
 *      Author: richard
 */

#ifndef HMM_SCORER_HH_
#define HMM_SCORER_HH_

#include <Core/CommonHeaders.hh>
#include <Math/Matrix.hh>
#include <Math/Vector.hh>
#include <Nn/NeuralNetwork.hh>

namespace Hmm {

class Scorer
{
private:
	static const Core::ParameterEnum paramScorerType_;
	enum ScorerType { framewiseNeuralNetworkScorer, gmmScorer };
protected:
	u32 nClasses_;
	bool isInitialized_;
	Math::Matrix<Float> const * sequence_;
public:
	Scorer();
	virtual ~Scorer() {}
	virtual void initialize();
	u32 nClasses() const { require(isInitialized_); return nClasses_; }
	virtual void setSequence(const Math::Matrix<Float>& sequence);
	virtual Float frameScore(u32 t, u32 c) { return 0.0; }
	virtual Float segmentScore(u32 t, u32 length, u32 c) { return 0.0; }

	/*
	 * factory
	 */
	static Scorer* create();
};


class FramewiseNeuralNetworkScorer : public Scorer
{
private:
	typedef Scorer Precursor;
	static const Core::ParameterString paramPriorFile_;
	static const Core::ParameterFloat paramPriorScale_;
	static const Core::ParameterBool paramLogarithmizeNetworkOutput_;
	static const Core::ParameterInt paramBatchSize_;
protected:
	std::string priorFile_;
	Float priorScale_;
	bool logarithmizeNetworkOutput_;
	u32 batchSize_;
	Nn::Vector prior_;
	Nn::NeuralNetwork network_;
	Nn::Matrix scores_;
public:
	FramewiseNeuralNetworkScorer();
	virtual ~FramewiseNeuralNetworkScorer() {}
	virtual void initialize();
	virtual void setSequence(const Math::Matrix<Float>& sequence);
	virtual Float frameScore(u32 t, u32 c);
};


class GaussianScorer : public Scorer
{
private:
	typedef Scorer Precursor;
	static const Core::ParameterString paramMeanFile_;
	static const Core::ParameterString paramVarFile_;
protected:
	u32 nDims_;
	std::string meanFile_;
	std::string varFile_;
	Math::Matrix<Float> mean_;
	Math::Matrix<Float> var_;
	Math::Vector<Float> gConst_;
	Math::Matrix<Float> scores_;
public:
	GaussianScorer();
	virtual ~GaussianScorer() {}
	virtual void initialize();
	virtual void setSequence(const Math::Matrix<Float>& sequence);
	virtual Float frameScore(u32 t, u32 c);
};

} // namespace

#endif /* HMM_SCORER_HH_ */
