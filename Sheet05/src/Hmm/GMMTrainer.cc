/*
 * FramewiseScorer.cc
 *
 *  Created on: May 12, 2017
 *      Author: richard
 */

#include "GMMTrainer.hh"

using namespace Hmm;

/*
 * GaussianScorer
 */
const Core::ParameterString GMMTrainer::paramMeanFile_("mean-file", "", "gmmTrainer");

const Core::ParameterString GMMTrainer::paramVarFile_("var-file", "", "gmmTrainer");

GMMTrainer::GMMTrainer() :
		nStates_(0),
		nDims_(0),
		meanFile_(Core::Configuration::config(paramMeanFile_)),
		varFile_(Core::Configuration::config(paramVarFile_))
{}

void GMMTrainer::applySequence( Features::SequenceFeatureReader& reader,  Features::SequenceLabelReader& labelReader) {

	nDims_ = reader.featureDimension();
	nStates_ = labelReader.featureDimension();

	mean_.resize(nDims_, nStates_);
	var_.resize(nDims_, nStates_);

	// compute mean and variance for each state

	for (u32 i_states=0; i_states < nStates_; i_states++ ){

		Math::Matrix<Float> feat_tmp;
		feat_tmp.resize( nDims_, 100000);
		feat_tmp.fill(0.0);

		Math::Vector<Float> feat_vec;
		u32 nFeat = 0;

		while (reader.hasSequences()) {

			Math::Matrix<Float> sequence = reader.next();
			std::vector<u32> labelSequence = labelReader.nextLabelSequence();


			// collect all elements
			for (u32 colIdx = 0; colIdx < sequence.nColumns(); colIdx++) {

				if (labelSequence.at(colIdx) == i_states){
					sequence.getColumn(colIdx, feat_vec);
					feat_tmp.setColumn(nFeat, feat_vec);
					nFeat++;
				}
			}
		}

		feat_tmp.resize(nDims_,nFeat);

		// get mean
		Math::Vector<Float> mean_vec;
		mean_vec.resize(nDims_, true);
		mean_vec.fill(0);
		for (u32 colIdx = 0; colIdx < feat_tmp.nColumns(); colIdx++) {
			feat_tmp.getColumn(colIdx, feat_vec);
			mean_vec.add(feat_vec);
		}
		mean_vec.divide(feat_tmp.nColumns());
		mean_.setColumn(i_states,  mean_vec);

		mean_vec.scale(-1);

		// get var
		Math::Vector<Float> var_vec;
		var_vec.resize(nDims_, true);
		var_vec.fill(0);
		for (u32 colIdx = 0; colIdx < feat_tmp.nColumns(); colIdx++) {

			feat_tmp.getColumn(colIdx, feat_vec);
			feat_vec.add(mean_vec);
			feat_vec.elementwiseMultiplication(feat_vec);
			var_vec.add(feat_vec);
		}
		var_vec.divide(feat_tmp.nColumns());
		var_.setColumn(i_states, var_vec);

		// reset feature reader
		reader.newEpoch();
		labelReader.newEpoch();

	}

	mean_.write(meanFile_, true);
	var_.write(varFile_, true);

}

