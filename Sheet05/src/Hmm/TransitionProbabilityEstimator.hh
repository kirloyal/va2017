/*
 * TransitionProbabilityEstimator.hh
 *
 *  Created on: Jun 30, 2017
 *      Author: richard
 */

#ifndef TRANSITIONPROBABILITYESTIMATOR_HH_
#define TRANSITIONPROBABILITYESTIMATOR_HH_

#include <Core/CommonHeaders.hh>
#include <Features/FeatureReader.hh>

namespace Hmm {

class TransitionProbabilityEstimator
{
private:
	static const Core::ParameterString paramTransitionProbabilityFile_;
	std::string file_;
	Features::SequenceLabelReader labelReader_;
public:
	TransitionProbabilityEstimator();
	~TransitionProbabilityEstimator() {}
	void estimate();
};

} // namespace

#endif /* TRANSITIONPROBABILITYESTIMATOR_HH_ */
