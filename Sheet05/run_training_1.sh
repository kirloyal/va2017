#!/bin/bash

mkdir -p log results

# train for 5 iterations
for i in 1 2 3 4 5
do
    echo "### ITERATION $i ###"
    ### ESTIMATE TRANSITION PROBABILITIES ###
    OPTIONS="--log-file=/dev/null \
             --action=estimate-transition-probabilities \
             --transition-probability-estimator.file=results/transition-probs.iter-$i.vector"
    if [ $i -eq 1 ]; then
        OPTIONS="$OPTIONS --features.label-reader.feature-cache=data/train.uniform.labels"
    else
        OPTIONS="$OPTIONS --features.label-reader.feature-cache=results/train.iter-$(( $i - 1 )).sequencelabels"
    fi
    ./src/Hmm/hmm-tool $OPTIONS

    ### TRAIN THE GMM ###
    OPTIONS="--log-file=log/gmm-hmm.train.iter-$i.log \
             --action=train-hmm \
             --gmmTrainer.mean-file=results/mean.iter-$i.matrix \
             --gmmTrainer.var-file=results/var.iter-$i.matrix \
             --features.feature-reader.feature-cache=data/train"
    # in first iteration use uniform alignment
    if [ $i -eq 1 ]; then
        OPTIONS="$OPTIONS --features.label-reader.feature-cache=data/train.uniform.labels"
    # in all other iterations use alignment based on previous iteration
    else
        OPTIONS="$OPTIONS --features.label-reader.feature-cache=results/train.iter-$(( $i - 1 )).sequencelabels"
    fi
	./src/Hmm/hmm-tool $OPTIONS

    ### EVALUATE RESULT OF THIS ITERATION ON THE TEST SET ###
    OPTIONS="--log-file=log/decode.iter-$i.log \
             --scorer.mean-file=results/mean.iter-$i.matrix \
             --scorer.var-file=results/var.iter-$i.matrix \
             --hidden-markov-model.transition-probability-file=results/transition-probs.iter-$i.vector \
             --features.label-writer.feature-cache=results/decoding.iter-$i.test.labels"
    ./src/Hmm/hmm-tool --config=config/decode_1.config $OPTIONS

    ./eval.sh data/test.labels results/decoding.iter-${i}.test.labels

	### REALIGNMENT: COMPUTE NEW HMM STATE ALIGNMENT FOR NEXT ITERATION ###
    OPTIONS="--log-file=log/realign.iter-$i.log \
             --scorer.mean-file=results/mean.iter-$i.matrix \
             --scorer.var-file=results/var.iter-$i.matrix \
             --hidden-markov-model.transition-probability-file=results/transition-probs.iter-$i.vector \
             --features.label-writer.feature-cache=results/train.iter-$i.sequencelabels"
	./src/Hmm/hmm-tool --config=config/realign_1.config $OPTIONS

done

