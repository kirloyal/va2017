/*
 * ViterbiDecoding.hh
 *
 *  Created on: May 10, 2017
 *      Author: richard
 */

#ifndef HMM_VITERBIDECODING_HH_
#define HMM_VITERBIDECODING_HH_

#include <Core/CommonHeaders.hh>
#include <Math/Vector.hh>
#include <Math/Matrix.hh>
//#include "Scorer.hh"
#include "HiddenMarkovModel.hh"

namespace Hmm {

class ViterbiDecoding
{

	/* the class for the actual Viterbi decoding */
private:
	static const Core::ParameterEnum paramViterbiOutput_;
	enum ViterbiOutput { hmmStates, labels };
protected:
	ViterbiOutput outputType_;
	HiddenMarkovModel* hmm_;
	std::vector<u32> segmentation_;
	std::vector<u32> framewiseRecognition_;
	bool isInitialized_;
private:
	void decodeFrame(u32 t, std::vector<u32>& oldHyp, std::vector<u32>& newHyp);
	void traceback(std::vector<u32>& hyp, u32 sequenceLength);
public:
	ViterbiDecoding();
	virtual ~ViterbiDecoding();
	void initialize();
	void sanityCheck();
	Float decode(const Math::Matrix<Float>& sequence);
	Float decodePath(const Math::Matrix<Float>& sequence, const std::vector<u32>& path);
	Float realign(const Math::Matrix<Float>& sequence, const std::vector<u32>& labelSequence);
	const std::vector<u32>& segmentation() const { return segmentation_; }
	const std::vector<u32>& framewiseRecognition() const { return framewiseRecognition_; }
	u32 nOutputClasses() const;
};

} // namespace

#endif /* HMM_VITERBIDECODING_HH_ */
