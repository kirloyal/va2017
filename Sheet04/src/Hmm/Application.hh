/*
 * Application.hh
 *
 *  Created on: Mar 03, 2015
 *      Author: richard
 */

#ifndef HMM_APPLICATION_HH_
#define HMM_APPLICATION_HH_

#include "Core/CommonHeaders.hh"
#include "Core/Application.hh"

namespace Hmm {

class Application: public Core::Application
{
private:
	static const Core::ParameterEnum paramAction_;
	enum Actions { none, inference, viterbiDecoding, realignment, trainHMM, evaluation };
public:
	virtual ~Application() {}
	virtual void main();
	void infer();
	void decode();
	void realign();
	void train();
	void eval();
};

} // namespace

#endif /* HMM_APPLICATION_HH_ */
