/*
 * Application.cc
 *
 *  Created on: Apr 10, 2014
 *      Author: richard
 */

#include "Application.hh"
//#include "GrammarGenerator.hh"
#include "ViterbiDecoding.hh"
#include "GMMTrainer.hh"
#include <Features/FeatureReader.hh>
#include <Features/FeatureWriter.hh>
#include <iostream>
#include <sstream>

using namespace Hmm;

APPLICATION(Hmm::Application)

const Core::ParameterEnum Application::paramAction_("action", "none, inference, viterbi-decoding, realignment, train-hmm, evaluation", "none");

void Application::main() {

	switch (Core::Configuration::config(paramAction_)) {
	case inference:
		infer();
		break;
	case viterbiDecoding:
		decode();
		break;
	case realignment:
		realign();
		break;
	case trainHMM:
		train();
		break;
	case evaluation:
		eval();
		break;
	case none:
	default:
		Core::Error::msg("No action given.") << Core::Error::abort;
	}
}

void Application::infer() {
    std::cout << std::endl << "Application::infer" << std::endl;
	// do something
    
    Features::SequenceFeatureReader readerFeature("features.sequence_feature_reader");
    std::cout << readerFeature.getCacheFilename() << std::endl;
    readerFeature.initialize();
    while (readerFeature.hasSequences()) 
    {
        std::cout << readerFeature.next() << std::endl;
    }
    ViterbiDecoding vd;
    vd.initialize();
    vd.sanityCheck();

}


void Application::decode() {


	// do something

}

void Application::realign() {

	// do something


}

void Application::train() {

	// do something

}

void Application::eval() {

	// do something

}
