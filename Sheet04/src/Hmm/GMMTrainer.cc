/*
 * FramewiseScorer.cc
 *
 *  Created on: May 12, 2017
 *      Author: richard
 */

#include "GMMTrainer.hh"

using namespace Hmm;


/*
 * GaussianScorer
 */
const Core::ParameterString GMMTrainer::paramMeanFile_("mean-file", "", "gmmTrainer");

const Core::ParameterString GMMTrainer::paramVarFile_("var-file", "", "gmmTrainer");

const Core::ParameterString GMMTrainer::paramTransFile_("trans-file", "", "gmmTrainer");

GMMTrainer::GMMTrainer() :
		meanFile_(Core::Configuration::config(paramMeanFile_)),
		varFile_(Core::Configuration::config(paramVarFile_)),
		transFile_(Core::Configuration::config(paramTransFile_))
{}

void GMMTrainer::initialize() {

	// do something

}

void GMMTrainer::applySequence( Features::SequenceFeatureReader& reader,  Features::SequenceLabelReader& labelReader) {

	// do something

}

