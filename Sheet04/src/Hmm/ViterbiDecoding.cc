/*
 * ViterbiDecoding.cc
 *
 *  Created on: May 10, 2017
 *      Author: richard
 */

#include "ViterbiDecoding.hh"

using namespace Hmm;

/*
 * ViterbiDecoding
 */
const Core::ParameterEnum ViterbiDecoding::paramViterbiOutput_("output", "hmm-states, labels", "labels", "viterbi-decoding");

ViterbiDecoding::ViterbiDecoding() :
		outputType_((ViterbiOutput) Core::Configuration::config(paramViterbiOutput_)),
		hmm_(0),
		isInitialized_(false)
{
}

ViterbiDecoding::~ViterbiDecoding() {
	delete hmm_;
}

void ViterbiDecoding::initialize() {
	hmm_ = HiddenMarkovModel::create();
	hmm_->initialize();
	isInitialized_ = true;
}

void ViterbiDecoding::sanityCheck() {
	// check number of hmm states
    std::cout << hmm_->nClasses() << "," << hmm_->nStates() << std::endl;
}

void ViterbiDecoding::decodeFrame(u32 t, std::vector<u32>& oldHyp, std::vector<u32>& newHyp) {

	// do something
    std::cout << "ViterbiDecoding::decodeFrame" << std::endl;
}

void ViterbiDecoding::traceback(std::vector<u32>& hyp, u32 sequenceLength) {

	// do something

}

Float ViterbiDecoding::decode(const Math::Matrix<Float>& sequence) {

	// do something

}

Float ViterbiDecoding::realign(const Math::Matrix<Float>& sequence, const std::vector<u32>& labelSequence) {

	// do something

}

u32 ViterbiDecoding::nOutputClasses() const {
	if (outputType_ == hmmStates)
		return hmm_->nStates();
	else // outputType_ == labels
		return hmm_->nClasses();
}
