/*
 * Stip.cc
 *
 *  Created on: Apr 25, 2017
 *      Author: richard
 */

#include "Stip.hh"

#define EPSILON 10E-11

using namespace ActionRecognition;

/* parameter definition */
const Core::ParameterString Stip::paramInputFile_(
		"file",    // parameter name
		"../../experiments/videos/edummy.avi",            // default value
		"stip");    // prefix (set parameter via --example.some-string=your-string)

// constructor
Stip::Stip() :		
	filename(Core::Configuration::config(paramInputFile_))
{
	std::cout << "RUN STIP" << std::endl;
	require(!filename.empty()); // make sure paramter filename contains some string
}

// empty destructor
Stip::~Stip()
{

}


#define SIGMA	3
#define TAU	2
#define K_SIGMA 15
#define K_TAU	15

void Stip::run()
{
	LoadVideo(filename);
//	PlayVideo(videoOriginal);
	
	Mat kernelX = getGaussianKernel(K_SIGMA, SIGMA, CV_32F);
	Mat kernelY = getGaussianKernel(K_SIGMA, SIGMA, CV_32F);
	Mat kernelT = getGaussianKernel(K_TAU, TAU, CV_32F);
	
	Smoothing(videoGray, videoSmoothen, kernelX, kernelY, kernelT);
//	PlayVideo(videoSmoothen);	
	Derivatives(videoSmoothen, videoIx, videoIy, videoIt);
//	PlayVideo(videoIx);
//	PlayVideo(videoIy);
//	PlayVideo(videoIt);
	Get2ndMoment(videoIx, videoIy, videoIt, videoIxIx, videoIyIy, videoItIt, videoIxIy, videoIyIt, videoIxIt);		
	GetHarris(videoHarris, videoIxIx, videoIyIy, videoItIt, videoIxIy, videoIyIt, videoIxIt);
//	PlayVideo(videoHarris,0,1);
	NonMaxSuppression(videoHarris, videoSTIP, pointsSTIP, 10);
	PlayVideoWithPoints(videoOriginal,pointsSTIP);
//	DisplayFrameWithPoints(videoOriginal,pointsSTIP);
	GetPolar(videoIx, videoIy, videoImag, videoIori);
	GetDescriptor(pointsSTIP, videoImag, videoIori, features);
	SaveDescriptor("output.txt",features);
}


void Stip::LoadVideo(string filename_)
{
	cout << "-- LoadVideo : " << filename_ << " --"  << endl;
	VideoCapture cap(filename_); // open the default camera
    	require(cap.isOpened());

	videoOriginal.clear();
	videoGray.clear();

        Mat frame;
	while(cap.read(frame))
	{
		Mat gray;
		cvtColor(frame, gray, CV_BGR2GRAY);
		gray.convertTo(gray, CV_32F);
		gray /= 255;
		videoOriginal.push_back(frame.clone());
		videoGray.push_back(gray.clone());

	}
	nFrame = videoOriginal.size();
	nRow = videoOriginal[0].rows;
	nCol = videoOriginal[0].cols;
	cout << "[ " << nRow << "rows X " << nCol << "cols X " 
		<< nFrame << "frames ]"  << endl;
}

void Stip::SaveDescriptor(string fileSave, vector< vector<float> > &data)
{
	ofstream write;
 	write.open(fileSave.c_str());
	write << data.size() << "\t" << data[0].size() << endl;
	for(int i=0;i<data.size();i++)
	{
		for(int j=0;j<data[i].size();j++)
		{
			write << data[i][j] << "\t";
		}
		write << endl;
	}
 	write.close();
}

void Stip::PlayVideo(vector<Mat> &in, int nWait, float scale)
{
	cout << "-- PlayVideo : x" << scale << " --" << endl;
	for(int i=0;i<in.size();i++)
	{
		imshow("Play",scale * in[i]);
		if(waitKey(30) >= 0) break;
	}
	waitKey(nWait);
}

void Stip::PlayVideoWithPoints(vector<Mat> &inVideo, vector< vector<Point> > &points, int nWait, float scale)
{
	cout << "-- PlayVideo : x" << scale << " with Points --" << endl;
	for(int i=0;i<inVideo.size();i++)
	{
		Mat frame = scale * inVideo[i];
		for(int j=0;j<points[i].size();j++)
		{
			cout << i << "," << j << ":" << points[i][j] <<endl;
			circle(frame, points[i][j], 5, Scalar(0, 0, 255), 1);
		}
		imshow("Play", frame);
		if(waitKey(30) >= 0) break;
	}
	waitKey(nWait);
}

void Stip::DisplayFrameWithPoints(vector<Mat> &inVideo, vector< vector<Point> > &points, int nWait, float scale)
{
	cout << "-- DisplayFrameWithPoints : x" << scale << " with Points --" << endl;
	for(int i=0;i<inVideo.size();i++)
	{
		if(points[i].size() == 0) continue;
		Mat frame = scale * inVideo[i];
		for(int j=0;j<points[i].size();j++)
		{
			cout << i << "," << j << ":" << points[i][j] <<endl;
			circle(frame, points[i][j], 5, Scalar(0, 0, 255), 1);
		}
		imshow("Play", frame);
		waitKey(nWait);
	}
}


void Stip::Smoothing(vector<Mat> &in, vector<Mat> &out, Mat kernelX, Mat kernelY, Mat kernelT)
{
	cout << "-- Smoothing --" << endl;
	out.clear();
	vector<Mat> spatial;
	spatial.clear();
	for(int i=0;i<in.size();i++)
	{
		Mat frame = in[i];
		Mat frameSpatialFiltered;
		sepFilter2D(frame, frameSpatialFiltered, -1, kernelX, kernelY);
		spatial.push_back(frameSpatialFiltered.clone());

	}
	int nIdxCenter = kernelT.rows/2;	
	for(int i=0;i<spatial.size();i++)
	{
		Mat smoothen = Mat::zeros(spatial[i].size(),spatial[i].type() );
		for(int j=0;j<kernelT.rows;j++)
		{
			int idxTarget = i + nIdxCenter - j;
			if(idxTarget < 0) idxTarget = 0;
			if(idxTarget > spatial.size()-1)
				idxTarget = spatial.size()-1;
			smoothen += kernelT.at<float>(j,0) * spatial[idxTarget];
		}
		out.push_back(smoothen.clone());
	}
	
}


void Stip::Derivatives(vector<Mat> &in, vector<Mat> &Ix, vector<Mat> &Iy, vector<Mat> &It)
{
	cout << "-- Derivatives --" << endl;
	Ix.clear();
	Iy.clear();
	It.clear();

	Mat kernel(1,3,CV_32F);
	kernel.at<float>(0,0) = -1;
	kernel.at<float>(0,1) = 0;
	kernel.at<float>(0,2) = 1;
	for(int i=0;i<in.size();i++)
	{
		Mat frameIx, frameIy;
		Mat frameIt = Mat::zeros(in[i].size(), in[i].type());
		filter2D(in[i], frameIx, -1, kernel);
		filter2D(in[i], frameIy, -1, kernel.t());
		for(int j=0;j<3;j++)
		{
			int idxTarget = i + j - 1;
			if(idxTarget < 0) idxTarget = 0;
			if(idxTarget > in.size()-1)
				idxTarget = in.size()-1;

			frameIt += kernel.at<float>(0,j) * in[idxTarget];
		}
		Ix.push_back(frameIx.clone());
		Iy.push_back(frameIy.clone());
		It.push_back(frameIt.clone());		
	}

}

void Stip::Get2ndMoment(vector<Mat> &Ix, vector<Mat> &Iy, vector<Mat> &It, vector<Mat> &IxIx, vector<Mat> &IyIy, vector<Mat> &ItIt, vector<Mat> &IxIy, vector<Mat> &IyIt, vector<Mat> &IxIt)
{
	cout << "-- Get2ndMoment --" << endl;
	vector<Mat> nonBlurredII;
	Mat kernelX = getGaussianKernel(K_SIGMA, SIGMA, CV_32F);
	Mat kernelY = getGaussianKernel(K_SIGMA, SIGMA, CV_32F);
	Mat kernelT = getGaussianKernel(K_TAU, TAU, CV_32F);
	
	nonBlurredII.clear();
	for(int i=0;i<Ix.size();i++)
	{
		nonBlurredII.push_back(Ix[i].mul(Ix[i]));
	}
	Smoothing(nonBlurredII, IxIx, kernelX, kernelY, kernelT);
	
	nonBlurredII.clear();
	for(int i=0;i<Ix.size();i++)
	{
		nonBlurredII.push_back(Iy[i].mul(Iy[i]));
	}
	Smoothing(nonBlurredII, IyIy, kernelX, kernelY, kernelT);
		
	nonBlurredII.clear();
	for(int i=0;i<Ix.size();i++)
	{
		nonBlurredII.push_back(It[i].mul(It[i]));
	}
	Smoothing(nonBlurredII, ItIt, kernelX, kernelY, kernelT);
	
	nonBlurredII.clear();
	for(int i=0;i<Ix.size();i++)
	{
		nonBlurredII.push_back(Ix[i].mul(Iy[i]));
	}
	Smoothing(nonBlurredII, IxIy, kernelX, kernelY, kernelT);
		
	nonBlurredII.clear();
	for(int i=0;i<Ix.size();i++)
	{
		nonBlurredII.push_back(Iy[i].mul(It[i]));
	}
	Smoothing(nonBlurredII, IyIt, kernelX, kernelY, kernelT);
	
	nonBlurredII.clear();
	for(int i=0;i<Ix.size();i++)
	{
		nonBlurredII.push_back(Ix[i].mul(It[i]));
	}
	Smoothing(nonBlurredII, IxIt, kernelX, kernelY, kernelT);

}


void Stip::GetHarris(vector<Mat> &H, vector<Mat> &IxIx, vector<Mat> &IyIy, vector<Mat> &ItIt, vector<Mat> &IxIy, vector<Mat> &IyIt, vector<Mat> &IxIt)
{
	cout << "-- GetHarris --" << endl;
	float k = 0.005;
	H.clear();
	for(int t=0;t<nFrame;t++)
	{
		Mat frameH = Mat::zeros(nRow,nCol,CV_32F);
		for(int y=0;y<nRow;y++)
		{
			for(int x=0;x<nCol;x++)
			{
				Mat mu(3,3,CV_32F);
				mu.at<float>(0,0) = IxIx[t].at<float>(y,x);
				mu.at<float>(0,1) = IxIy[t].at<float>(y,x);
				mu.at<float>(0,2) = IxIt[t].at<float>(y,x);
				mu.at<float>(1,0) = IxIy[t].at<float>(y,x);
				mu.at<float>(1,1) = IyIy[t].at<float>(y,x);
				mu.at<float>(1,2) = IyIt[t].at<float>(y,x);
				mu.at<float>(2,0) = IxIt[t].at<float>(y,x);
				mu.at<float>(2,1) = IyIt[t].at<float>(y,x);
				mu.at<float>(2,2) = IyIy[t].at<float>(y,x);
				frameH.at<float>(y,x) = determinant(mu) - k*pow(trace(mu).val[0],3);	
			}
		}
		H.push_back(frameH);
	}
	
}

void Stip::NonMaxSuppression(vector<Mat> &in, vector<Mat> &out, vector< vector<Point> > &points, int r)
{
	cout << "-- NonMaxSuppression --" << endl; 
	out.clear();
	points.clear();
	for(int i=0;i<in.size();i++)
	{
		Mat frame;
		threshold(in[i], frame, 0, 1, THRESH_BINARY); 	
		//threshold(in[i], frame, EPSILON, 1, THRESH_BINARY); 	
		out.push_back(frame);
	}
	for(int t=0 ; t<out.size() ; t++)
	{
		vector<Point> pointsInFrame;
		pointsInFrame.clear();
		for(int y=0;y < out[t].rows;y++)
		{
			for(int x=0 ; x < out[t].cols ; x++)
			{
				float valCurr = in[t].at<float>(y,x);
				for(int wt = t-r ; wt <= t+r && out[t].at<float>(y,x) == 1; wt++)
				{
					if( wt < 0 || wt > out.size()-1 ) continue;
					for(int wy = y-r ; wy <= y+r && out[t].at<float>(y,x) == 1 ; wy++)
					{
						if( wy < 0 || wy > out[t].rows-1 ) continue;	
						for(int wx = x-r ; wx <= x+r && out[t].at<float>(y,x) == 1 ; wx++)
						{
							if( wx < 0 || wx > out[t].cols-1 || (x==wx && y==wy && t==wt) ) continue;
							{
								//if(valCurr < in[wt].at<float>(wy,wx))
								if(valCurr < in[wt].at<float>(wy,wx) + EPSILON)
								{
									out[t].at<float>(y,x) = 0;
								}
								else
								{
									out[wt].at<float>(wy,wx) = 0;
								}
							}	
						}
						
					}
				}
				if(out[t].at<float>(y,x)==1) 
				{
				//	cout << t << "," << x << "," << y << endl;
					Point point;
					point.x=x;
					point.y=y;
					pointsInFrame.push_back(point);
				}
			}
		}
		points.push_back(pointsInFrame);
	}

}
	
void Stip::GetDescriptor(vector< vector<Point> > &points, vector<Mat> &videoMag, vector<Mat> &videoOri, vector< vector<float> > &out)
{
	out.clear();
	Point3i size;
	Point3i nCell;
	nCell.x = 3;
	nCell.y = 3;
	nCell.z = 2;
	int k = 9;
	size.x = 2*k*SIGMA;
	size.y = 2*k*SIGMA;
	size.z = 2*k*TAU;
	for(int i=0;i<points.size();i++)
	{
		for(int j=0;j<points[i].size();j++)
		{
			Point3i center(points[i][j].x, points[i][j].y, i);
			vector<float> HOF;			
			GetHOF(center, size, nCell, videoImag, videoIori, HOF, 4);
			out.push_back(HOF);
		}
	}

}
	
void Stip::GetPolar(vector<Mat> &videoX, vector<Mat> &videoY, vector<Mat> &videoMag, vector<Mat> &videoOri, bool bDeg)
{
	videoMag.clear();
	videoOri.clear();
	for(int i=0;i<videoX.size();i++)
	{
		Mat mag,ori;
		cartToPolar(videoX[i], videoY[i], mag, ori, bDeg);
		videoMag.push_back(mag);
		videoOri.push_back(ori);
	}
}

void Stip::GetHOF(Point3i pointCenter, Point3i sizeVolume, Point3i nCell, vector<Mat> &videoImag, vector<Mat> &videoIori, vector<float> &out, int nBin)
{
	float binsize = 180.0/nBin;
	int nAllCells = nCell.x * nCell.y * nCell.z;
	out.resize(nBin*nAllCells, 0);


	vector< vector<float> > bins;
	bins.clear();
	for(int i=0;i<nAllCells;i++)
	{
		vector<float> cell(nBin, 0);
		bins.push_back(cell);
	}
	
	for(int t = pointCenter.z-sizeVolume.z/2 ; t <= pointCenter.z+sizeVolume.z/2 ; t++)
	{	
		if(t<0 || t>=videoImag.size()) continue;
		int idxT = nCell.z * ( t - (pointCenter.z-sizeVolume.z/2) ) / (sizeVolume.z+1);
		for(int y = pointCenter.y-sizeVolume.y/2 ; y <= pointCenter.y+sizeVolume.y/2 ; y++)
		{
			if(y<0 || y>=videoImag[t].rows) continue;
			int idxY = nCell.y * ( y - (pointCenter.y-sizeVolume.y/2) ) / (sizeVolume.y+1);
			for(int x = pointCenter.x-sizeVolume.x/2 ; x <= pointCenter.x+sizeVolume.x/2 ; x++)
			{
				if(x<0 || x>=videoImag[t].cols) continue;
				int idxX = nCell.x * ( x - (pointCenter.x-sizeVolume.x/2) ) / (sizeVolume.x+1);

				int idxBin = idxT * nCell.x * nCell.y + idxY * nCell.x + idxX;
				float oriScaled = fmod( videoIori[t].at<float>(y,x) , 180);
				int idxA = oriScaled/binsize; 
				int idxB = (idxA+1)%nBin;
				float weightB = oriScaled/binsize - idxA;
				float weightA = 1.0 - weightB;
				bins[idxBin][idxA] += weightA*videoImag[t].at<float>(y,x);
				bins[idxBin][idxB] += weightB*videoImag[t].at<float>(y,x);
			}
		}
	}
	for(int i=0;i<nAllCells;i++)
	{
		float L2 = 0;
		for(int j=0;j<bins[i].size();j++)
		{
			L2 += bins[i][j]*bins[i][j];
		}
		
		L2 = sqrt(L2);
		for(int j=0;j<bins[i].size();j++)
		{
			out[i*nBin+j] = bins[i][j] / L2;	
		}
	}

}

	
