/*
 * Stip.hh
 *
 *  Created on: Apr 25, 2017
 *      Author: richard
 */

#ifndef ACTIONRECOGNITION_STIP_HH_
#define ACTIONRECOGNITION_STIP_HH_

#include "Core/CommonHeaders.hh" // Core::Log, Core::Error, Core::Configuration, Types
#include "Math/Matrix.hh"
#include "Math/Vector.hh"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

namespace ActionRecognition {

class Stip
{
private:
	static const Core::ParameterString paramInputFile_;
	string filename;
	int nRow;
	int nCol;
	int nFrame;
	vector<Mat> videoOriginal;
	vector<Mat> videoGray;
	vector<Mat> videoSmoothen;
	vector<Mat> videoIx;
	vector<Mat> videoIy;
	vector<Mat> videoIt;
	vector<Mat> videoIxIx;
	vector<Mat> videoIyIy;
	vector<Mat> videoItIt;
	vector<Mat> videoIxIy;
	vector<Mat> videoIyIt;
	vector<Mat> videoIxIt;
	vector<Mat> videoHarris;
	vector<Mat> videoSTIP;
	vector< vector<Point> > pointsSTIP;
	vector<Mat> videoImag;
	vector<Mat> videoIori;
	vector< vector<float> > features;
public:
	Stip();
	virtual ~Stip();
	void run();

	void LoadVideo(string filename_);
	void SaveDescriptor(string fileSave, vector< vector<float> > &data);
	void PlayVideo(vector<Mat> &in, int nWait=0, float scale=1.0);
	void PlayVideoWithPoints(vector<Mat> &inVideo, vector< vector<Point> > &points, int nWait=0, float scale=1.0);
	void DisplayFrameWithPoints(vector<Mat> &inVideo, vector< vector<Point> > &points, int nWait=0, float scale=1.0);
	
	void Smoothing(vector<Mat> &in, vector<Mat> &out, Mat kernelX, Mat kernelY, Mat kernelT);
	void Derivatives(vector<Mat> &in, vector<Mat> &Ix, vector<Mat> &Iy, vector<Mat> &It);
	void Get2ndMoment(vector<Mat> &Ix, vector<Mat> &Iy, vector<Mat> &It, vector<Mat> &IxIx, vector<Mat> &IyIy, vector<Mat> &ItIt, vector<Mat> &IxIy, vector<Mat> &IyIt, vector<Mat> &IxIt);
	void GetHarris(vector<Mat> &H, vector<Mat> &IxIx, vector<Mat> &IyIy, vector<Mat> &ItIt, vector<Mat> &IxIy, vector<Mat> &IyIt, vector<Mat> &IxIt);
	void NonMaxSuppression(vector<Mat> &in, vector<Mat> &out, vector< vector<Point> > &points, int r=3);
	void GetDescriptor(vector< vector<Point> > &points, vector<Mat> &videoMag, vector<Mat> &videoOri, vector< vector<float> > &out);
	void GetPolar(vector<Mat> &videoX, vector<Mat> &videoY, vector<Mat> &videoMag, vector<Mat> &videoOri, bool bDeg=true);
	void GetHOF(Point3i pointCenter, Point3i sizeVolume, Point3i nCell, vector<Mat> &videoImag, vector<Mat> &videoIori, vector<float> &out, int nBin=4);



};

} // namespace


#endif /* ACTIONRECOGNITION_STIP_HH_ */
